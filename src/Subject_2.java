import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Subject_2 {
    private JButton Invert;
    private JPanel panel1;
    private JTextField textField1;

    public Subject_2() {
        Invert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String text;
                String reverse = null;
                text = textField1.getText();

                for(int i = text.length() - 1; i >= 0; i--)
                {
                    reverse = reverse + text.charAt(i);
                }

                System.out.println("Reversed string is:");
                System.out.println(reverse);
            }
        });
    }
}
